# FreeDOS Release and Installation Media Builder

Automation tools for creating a [FreeDOS](http://www.freedos.org) release and
it's associated install media.

It is designed to be used with [VirtualBox](https://www.virtualbox.org) and
[openSUSE](https://www.opensuse.org) Leap.

_Other methods for creating a release may work. But, they are not officially
supported and will most likely create security issues on the building host.
Therefor, other methods are discouraged. This is not end-user software. It is
strictly a build environment/compiler for the end user software (aka FreeDOS).
The host OS used to build the release is not relevant. It could change at any
point to continue to provide a free and easy release build platform for the
operating system installation media._

***
### Install the FreeDOS builder tools.

	sudo ./install

This will automatically apply any updates to all required system packages and
download any additionally software needed to build a FreeDOS release. It will
also configure the system and prepare a the web server for downloading the
created release media and files. This can be/should be run anytime you wish to
apply system or RBE updates to insure the system remains configured for
building release media.
