#!/bin/bash

[[ ! ${tools} ]] && export tools=tools

. ${tools}/common

if [[ -d ${temp}/slices ]] ; then
	rm -rf ${temp}/slices
	if [[ -f ${cache}/slices ]] ; then
		rm -f ${cache}/slices
	fi
fi

test -d ${cache}/slices && exit 0

new_release_needed

function make_fd_x86_pkgs () {

	local ret=0

	echo Create ${cfg_platform} ${cfg_version} x86 Floppy Edition binary and data files

	if [[ -e ${temp}/drive.img ]] ; then
		rm -f ${temp}/drive.img || return 1
	fi

	make_blank_hd drive.img 50M format-drive || return 1

    if [[ -d ${temp}/install ]] ; then
		rm -rf ${temp}/install || return 1
	fi

	cleandir ${temp}/install || return $?

	echo Place x86 packages
	pkginst_all ${lists}/x86_all.lst || return 1

	ddir=$(lowerCase "${cfg_dosdir}")
	ddir="${swd}/${temp}/install/freecom/${ddir}"

	mkdir -p ${ddir}/bin || return 1
	if [[ "${cfg_command}" == "" ]] ; then
		pkgfiles freecom -d ${ddir}/bin bin/command.com || return 1
	else
		t=$(lowerCase "${cfg_command}")
		echo "using alternate COMMAND shell '${t}'"
		pkgfiles "${t%%,*}" -d ${ddir}/bin -o command.com "${t#*,}" || return 1
	fi

	# relocate fdnet if installed to dosdir/bin
	ddir="${swd}/${temp}/install/fdnet/${ddir##*/}"
	t="${swd}/${temp}/install/fdnet"

	if [[ -e ${t}/net/fdnet ]] ; then
		mv -f ${t}/net/fdnet ${ddir}/bin/network || die
	fi
	if [[ -e ${t}/network/fdnet ]] ; then
		mv -f ${t}/network/fdnet ${ddir}/bin/network || die
	fi

	${tools}/mnt drive || return 1

	mv -f $(isVerbose) ${temp}/install ${mnt}/drive/packages || ret=$?

	${tools}/mnt -u drive || return 1
	[[ $ret -ne 0 ]] && return $ret

	return 0
}

function make_msg_files () {

	local line
	local lang=$(lowerCase "${1##*.}")
	rm $(isVerbose -v) "${temp}/install/msg-${lang}".* 2>/dev/null
	local index=0
	local flag
	while [[ true ]] ; do
		[[ ${flag} ]] && break
		read -r line || flag=last
		line="${line//[$'\t\r\n']}"
		if [[ "${line// }" == "*" ]] ; then
			((index++))
			continue
		fi;
		echo "${line}" | set_text_vars >>"${temp}/install/msg-${lang}.${index}"
	done<"${1}"
	# bbedit shell context highlighting bug "

	return 0
}


function make_x86_texts () {

	local f e n p
	echo ${1}
	p=$(fileCase -a "${1}")
	echo ${p}
	for f in "${p}"/* ; do
		[[ ! -f "${f}" ]] && continue
		n=$(lowerCase "${f##*/}")
		[[ "${n//utf}" != "${n}" ]] && continue
		e="${n##*.}"
		n="${n%.*}"
		if [[ "${n}" == 'messages' ]] ; then
			make_msg_files "${f}" || return 1
			continue
		fi
		[[ "${n}" != 'header' ]] && [[ "${n}" != 'footer' ]] && continue
		[[ "${e}" == "txt" ]] && e="en"
		modcp "${f}" "${temp}/install/${n}.${e}" || return 1
	done

	return 0

}

function make_x86_messages () {

	local ret
	echo Make x86 messages

	cleandir ${temp}/install || return $?

	make_x86_texts "${fdi_x86}/SETTINGS" || return 1
	make_x86_texts "${fd_nls}/FDI-x86/MESSAGES" || return 1

	${tools}/mnt drive || return 1

	mv -f $(isVerbose) ${temp}/install/* ${mnt}/drive/ || ret=$?

	${tools}/mnt -u drive || return 1

	return $ret

}

function make_slices () {

	local pcnt=0
	local mcnt=0
	local midx=0
	local pidx=0
	local pkg
	local line
	local grps
	local tags=','
	local tag
	local d l
	local ptgz

	rm -rf "${mnt}/drive/${cfg_platform}".* 1>/dev/null 2>&1

	for d in ${mnt}/drive/packages/*/${cfg_dosdir} ; do
		pkg="${d%/*}"
		pkg="${pkg##*/}"
		[[ -f "${d}/appinfo/${pkg}.lsm" ]] && (( pcnt++ ))
	done

	# echo ${pcnt} >&2
	for d in ${mnt}/drive/msg-*.* ; do
		[[ ! -f "${d}" ]] && continue
		[[ "${verbose}" == "yes" ]] && echo ${d} >&2
		pkg="${d##*.}"
		(( pkg++ ))
		[[ ${pkg} -gt ${mcnt} ]] && mcnt=${pkg}
	done

	echo $pcnt packages - $mcnt messages >&2

	echo "@echo off"
	echo "if not exist C:\\TEMP\\NUL mkdir C:\\TEMP"
	echo "set TEMP=C:\\TEMP"
	echo "D:"
	echo "vecho /p /fWhite Create ${cfg_platform} ${cfg_version} slicer archive. /fGray"
	echo "vecho ${slice_size} slices, ${pcnt} packages, ${mcnt} messages) /p"

	echo "vpause /t 2"
	# echo "command.com"

	if [[ -f "${mnt}/drive/header.txt" ]] ; then
		echo "slicer /c /f ${cfg_platform} /s ${slice_size} /m header.txt"
	else
		echo "slicer /c /f ${cfg_platform} /s ${slice_size}"
	fi

	for d in ${mnt}/drive/header.* ; do
		if [[ -f "${d}" ]] ; then
			l="${d##*.}"
			[[ "${l}" == 'txt' ]] && continue
			echo "slicer /rq /f \\${cfg_platform} /L ${l} /m ${d##*/}"
			echo "if errorlevel 1 goto end"
		fi
	done

	[[ ${mcnt} -gt 0 ]] && pcnt=$(( ${pcnt} / ${mcnt} ))
	mcnt=${pcnt}

	while [[ true ]] ; do
		[[ ${flag} ]] && break
		read -r line || flag=last
		line="${line//[$'\t\r\n']}"
		line="${line#${line%%[![:space:]]*}}"
		line="${line%${line##*[![:space:]]}}"
		[[ ! ${line} ]] && continue
		[[ "${line:0:1}" == ';' ]] && continue
		if [[ "${line:0:7}" == "# tags:" ]] ; then
			line="${line:7}"
			while [[ "${line}" != '' ]] ; do
				line="${line#${line%%[![:space:]]*}}"
				if [[ "${line:0:1}" == '+' ]] ; then
					line="${line:1}"
					tag="${line%%,*}"
					line="${line:${#tag}}"
					line="${line:1}"
					tags="${tags}${tag},"
					grps="${tags#*,}"
					grps="${grps%,*}"
				elif [[ "${line:0:1}" == '-' ]] ; then
					line="${line:1}"
					tag="${line%%,*}"
					line="${line:${#tag}}"
					line="${line:1}"
					tags="${tags//,${tag},/,}"
					grps="${tags#*,}"
					grps="${grps%,*}"
				elif [[ "${line}" != '' ]] ; then
					echo "'${line%%,*}' tag missing preceding -/+ directive in ${cache}/lists/x86_all.lst" | errorlog >&2
					break
				fi
			done
			continue
		fi

		[[ "${line}" == "" ]] && continue
		[[ "${line:0:1}" == "#" ]] && continue

		(( pidx ++ ))
		pkg="${line##*\\}"
		echo "rem ${pidx} ${pkg} - ${mcnt}/${pcnt}"

		for d in ${mnt}/drive/${pkg^^}.* ${mnt}/drive/${pkg,,}.* ; do
			[[ ! -f "${d}" ]] && continue
			d="${d,,}"
			l="${d##*.}"
			[[ "$l" == "msg" ]] && l="*"
			echo "slicer /rq /f \\${cfg_platform} /g ${grps},${pkg} /L ${l} /m ${pkg,,}.${d##*.}"
			echo "if errorlevel 1 goto end"
		done

		echo "if not exist packages\\${pkg}\\${cfg_dosdir}\\nul goto Skip${pkg}"
		echo "cd packages\\${pkg}\\${cfg_dosdir}"
		echo "slicer ${ptgz}/ra /f \\${cfg_platform} /g ${grps},${pkg} /e SOURCE /i *.* "
		echo "if errorlevel 1 goto end"
		echo "cd \\"
		echo ":Skip${pkg}"

		if [[ "${slice_compress}" == "yes" ]] && [[ "${pkg}" == 'gzip' ]] ; then
			ptgz='/p gz '
		fi

		(( mcnt++ ))
		if [[ ! ${mcnt} -lt ${pcnt} ]] ; then
			mcnt=0
			for d in ${mnt}/drive/msg-*.${midx} ; do
				if [[ -f "${d}" ]] ; then
					l="${d##*-}"
					l="${l%%.*}"
					echo "slicer /rq /f \\${cfg_platform} /g ${grps},${pkg} /L ${l} /m msg-${l^^}.${midx}"
					echo "if errorlevel 1 goto end"
				fi
			done
			(( midx++ ))
		fi;

	done<"${cache}/lists/x86_all.lst"
	# bbedit shell context highlighting bug "

	for d in ${mnt}/drive/footer.* ; do
		if [[ -f "${d}" ]] ; then
			l="${d##*.}"
			[[ "${l}" == 'txt' ]] && continue
			echo "slicer /rq /f \\${cfg_platform} /L ${l} /m ${d##*/}"
			echo "if errorlevel 1 goto end"
		fi
	done

	if [[ -f "${mnt}/drive/footer.txt" ]] ; then
		echo "slicer /rq /f ${cfg_platform} /L * /m footer.txt"
	fi

	echo ":end"
	echo "if not errorlevel 1 goto done"
	echo "cd \\"
	echo "vpause /t 10"
	echo "verrlvl 1"
	echo ":done"

	return 0
}

function make_slicer_bat () {
	echo "Making ${cfg_platform} Floppy Edition slices, this takes a while"
	[[ "${slice_compress}" ]] && echo "Compression enabled, it will take a long while like maybe 5-10 minutes."
	local ret
	${tools}/mnt drive || return 1
	make_slices >"${mnt}/drive/MKSLICES.BAT" || ret=$?
	${tools}/mnt -u drive || return 1
	[[ $ret -ne 0 ]] && return $ret
	# verbose=yes
	run_task make-slices drive || return 1
	# verbose=no
}

function retrieve_slices () {
	if [[ -d "${temp}/slices" ]] ; then
		rm -rf "${temp}/slices" || return 1
	fi
	mkdir -p $(isVerbose) ${temp}/slices
	local ret=0
	local f n
	local p=$(lowerCase "${cfg_platform}")
	${tools}/mnt drive || return 1
	for f in "${mnt}/drive/"* ; do
		[[ ! -f "${f}" ]] && continue
		n=$(lowerCase "${f##*/}")
		[[ "${n%.*}" != "${p}" ]] && continue
		cp -f $(isVerbose) "${f}" "${temp}/slices/${n}" || ret=$?
		[[ $ret -ne 0 ]] && break
	done
	${tools}/mnt -u drive || return 1

	return $ret
}

function slices_process () {
	if [[ -d ${temp}/slices ]] ; then
		rm -rf ${temp}/slices
		if [[ -f ${cache}/slices ]] ; then
			rm -f ${cache}/slices
		fi
	fi

	make_fd_x86_pkgs || return 1

	make_x86_messages || return 1

	make_slicer_bat || return 1

	retrieve_slices || return 1

	rm -rf ${temp}/drive.img || return 1

	mv -f $(isVerbose) ${temp}/slices ${cache}/slices || return 1
}

# for some reason, unless you watch qemu the job sometimes timesout. It seems to
# run much slower or possibly freezes. So we will try more than once and on second
# attempt we will switch to verbose mode.
function run_slices_process () {
	local hv="${verbose}"
	while [[ true ]] ; do
		slices_process && break
		[[ "${verbose}" == "yes" ]] && return 1
		echo "failed first attempt" | errorlog
		echo "switching to verbose mode and trying again" | errorlog
		verbose=yes
	done
	verbose="${hv}"
}

hold_verbose="${verbose}"
# [[ "${slice_compress}" == 'yes' ]] && verbose=yes
run_slices_process
verbose="${hold_verbose}"

exit $?