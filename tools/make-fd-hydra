#!/bin/bash

[[ ! ${tools} ]] && export tools=tools

. ${tools}/common

if [[ -f ${temp}/fd-hydra.iso ]] ; then
	rm -rf ${temp}/fd-hydra.iso
	if [[ -f ${cache}/fd-hydra.iso ]] ; then
		rm -f ${cache}/fd-hydra.iso
	fi
fi

test -f ${cache}/fd-hydra.iso && exit 0

new_release_needed

function make_fd_hydra () {

	echo Create ${cfg_platform} ${cfg_version} hydra cd-rom iso \'${cfg_prefix}-LIVE\'

    if [[ -d ${temp}/iso ]] ; then
		rm -rf ${temp}/iso || return 1
	fi

    if [[ -d ${temp}/install ]] ; then
		rm -rf ${temp}/install || return 1
	fi

    if [[ -d ${temp}/isolinux ]] ; then
		rm -rf ${temp}/isolinux || return 1
	fi

	cleandir ${temp}/install || return $?

	fdinst "boot/syslnx.zip" || return 1

	local f=$(find ${temp}/install | grep -i '/ISOLINUX.BIN' 2>/dev/null)
	if [[ "${f}" == '' ]] ; then
		echo "error: unable to locate isolinux files" | errorlog
		return 1
	fi

	f="${f%/*}"
	mv -f $(isVerbose) "${f}"	 "${temp}/isolinux" || return 1
	cleandir ${temp}/install || return $?

	pkginst_all ${lists}/pkg_live.lst || return 1
	rm -rf ${temp}/install/*/fdos/source || return 1
	pkg_consolidate all_files || return 1

	mv -f $(isVerbose) "${temp}/isolinux" "${temp}/install/isolinux" || return 1

	local extras=",${cfg_add_extra,,},"

	cp -f ${cache}/fd-floppy.img ${temp}/floppy.img || return 1
	${tools}/mnt floppy || return 1
	cp -fa $(isVerbose) ${mnt}/floppy/* ${temp}/install/ || ret=$?
	${tools}/mnt -u floppy || return 1
	[[ $ret -ne 0 ]] && return $ret
	rm -rf ${temp}/floppy.img || return 1

	cp -f ${cache}/boot-hydra.img ${temp}/floppy.img || return 1
	${tools}/mnt floppy || return 1
	cp -fa $(isVerbose) $(fileCase "${mnt}/floppy/fdauto.bat") ${temp}/install/ || ret=$?
	cp -fa $(isVerbose) $(fileCase "${mnt}/floppy/fdconfig.sys") ${temp}/install/ || ret=$?
	mkdir -p ${temp}/install/freedos/bin || ret=$?
	cp -fa $(isVerbose) $(fileCase "${mnt}/floppy/freedos/bin/fdlive.bat") ${temp}/install/freedos/bin || ret=$?
	${tools}/mnt -u floppy || return 1
	[[ $ret -ne 0 ]] && return $ret
	rm -rf ${temp}/floppy.img || return 1

	mv -f $(isVerbose) ${temp}/install ${temp}/iso || return 1

	pkgcp ${lists}/pkg_base.lst ${temp}/iso/packages || return 1
	pkgcp ${lists}/pkg_live.lst ${temp}/iso/packages || return 1
	pkgcp ${lists}/pkg_full.lst ${temp}/iso/packages || return 1

	if [[ "${extras//,live,}" != "${extras}" ]] ; then
		pkgcp ${lists}/pkg_xtra.lst ${temp}/iso/packages || return 1
	fi

	metacp ${temp}/iso/packages || return 1

	add_x86 live ${temp}/iso || return 1

	cp -f $(isVerbose) ${cache}/boot-standard.img ${temp}/iso/isolinux/fdinst.img || return 1
	cp -f $(isVerbose) ${cache}/boot-hydra.img ${temp}/iso/isolinux/fdlive.img || return 1
	if [[ -f ${cache}/fd-x86.img ]] ; then
		cp -f $(isVerbose) ${cache}/fd-x86.img ${temp}/iso/isolinux/fdx86.img || return 1
	fi

	modcp ${tools}/isolinux/live.cfg ${temp}/iso/isolinux/isolinux.cfg || return 1

	cp -f $(fileCase "${temp}/iso/command.com") ${temp}/iso/freedos/bin/command.com  || return 1

	cd ${temp}/iso || return 1

	mkisofs $(notVerbose -quiet) -o ../fd-hydra.iso -V "${cfg_prefix}-LiveCD" -r -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table . || return 1

	cd "${swd}" || return 1

	rm -rf ${temp}/iso || return 1

	check_iso_size "${temp}/fd-hydra.iso" || return 1

	return 0

}

make_fd_hydra || die

mv -f $(isVerbose) ${temp}/fd-hydra.iso ${cache}/fd-hydra.iso || die

exit $?